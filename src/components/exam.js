import React,{useState} from 'react';
import Modal from './modal'
import './exam.css'
const Exam  = ()=>{
    const [examData] = useState([
        {'question':'Which year we got independence ?','option1':'1947','option2':'1950','option3':'1945','option4':'1955'},
        {'question':'Which year we got Republic ?','option1':'1947','option2':'1950','option3':'1945','option4':'1955'},
        {'question':'Our First Prime minister?','option1':'Gandhi','option2':'Nehru','option3':'Sardhar','option4':'Modi'},
        {'question':'Who won first Ipl title?','option1':'RR','option2':'MI','option3':'CSK','option4':'RCB'},
        // {'question':'Microsoft CEO ?','option1':'Sundar','option2':'Satya','option3':'Billgates','option4':'Jeff'},
        // {'question':'Google CEO ?','option1':'Sundar','option2':'Satya','option3':'Billgates','option4':'Jeff'},
    ])
    const [showModal,setShowModal] =useState(false)
    return (
        <div className="exam-container">
                {examData.map((dataItem,index)=>{
                    return (    
                        <div className="exam-question" key={index}>
                            <p>{dataItem.question}</p>
                            <div className="options">
                                <input type="radio" name={dataItem.question} value={dataItem.option1} />
                                <label>{dataItem.option1}</label>
                                <input type="radio" name={dataItem.question} value={dataItem.option2} />
                                <label>{dataItem.option2}</label>
                                <input type="radio" name={dataItem.question} value={dataItem.option3} />
                                <label>{dataItem.option3}</label>
                                <input type="radio" name={dataItem.question} value={dataItem.option4} />
                                <label>{dataItem.option4}</label>
                            </div>
                        </div>
                    )
                })}
                <button className="submit-btn" onClick={()=>setShowModal(true)}>Submit</button>
                {showModal ? <Modal type="success"/> : null}
        </div>
    )
}
export default Exam;