import React from "react";
// const App = (props) => {
//   const videoRef = useRef(null);
//   const errorMessage = useRef(null);
//   useEffect(() => {
//     getVideo();
//   },[]);

//   const getVideo = () => {
//     navigator.mediaDevices
//       .getUserMedia({video: {width:200,height:100} })
//       .then(stream => {
//         let video = videoRef.current;
//         video.srcObject = stream;
//         video.play();
//         props.setShowButton();
//       })
//       .catch(err => {
//         if(err instanceof TypeError){
//           console.error("error:", err);
//         }
//         else{
//           errorMessage.current.innerHTML = "You denied permission..enable it or else you can't write"
//         }
//       });
//   };
//   // const takeScreenshot = ()=>{
//   //   console.log('taking screen shot....')
//   //   const canvas = document.getElementById("canvas")
//   //   let context = canvas.getContext('2d')
//   //   // height = videoRef.current.videoHeight / (video.videoWidth/width);
//   //   // console.log(videoRef.current.videoHeight)
//   //   context.drawImage(videoRef.current,0,0,200,100);
//   //   let data = canvas.toDataURL('image/png');
//   //   // const img = document.createElement('img');
//   //   // img.setAttribute('src', data);
//   //   // document.getElementById('images').prepend(img)
//   //   const link = document.createElement('a')
//   //   link.href = data
//   //   link.download = 'image file name here'
//   //   document.body.appendChild(link)
//   //   link.click()
//   //   document.body.removeChild(link)
//   // }
//   return (
//       <div>
//         {/* <button>Take a photo</button> */}
//         <video ref={videoRef} style={{'margin':'10px 0 0 10px'}}/>
//         {/* <button onClick={()=>takeScreenshot()} style={{'padding':'5px 20px','border':'1px solid black','borderRadius':'24px','margin':'10px 0 0 130px'}}>Take Picture</button> */}
//         <canvas id="canvas" width="200" height="100" style={{'display':'none'}}></canvas>
//         <p ref={errorMessage} style={{'color':'red'}}></p>
//       </div>
//   );
// };

// export default App;
class Video extends React.Component {
  state = { height: 200, width: 300 }
  componentDidMount() {
    this.getVideo()
  }
  getVideo = () => {
    navigator.mediaDevices
      .getUserMedia({ video: { width: this.state.width, height: this.state.height } })
      .then(stream => {
        let video = document.getElementById('video');
        video.srcObject = stream;
        video.play();
        if (this.props.setShowButton) { this.props.setShowButton(); }
      })
      .catch(err => {
        document.getElementById("errorMessage").innerHTML = "You denied permission..enable it from url box"
      });
  }
  takeScreenshot = () => {
    console.log('taking screen shot....')
    const canvas = document.getElementById("canvas")
    let context = canvas.getContext('2d')
    // height = videoRef.current.videoHeight / (video.videoWidth/width);
    // console.log(videoRef.current.videoHeight)
    context.drawImage(document.getElementById('video'), 0, 0, this.state.width, this.state.height);
    let data = canvas.toDataURL('image/jpeg');
    // const img = document.createElement('img');
    // img.setAttribute('src', data);
    // document.getElementById('images').prepend(img)
    const link = document.createElement('a')
    link.href = data
    link.download = this.state.width.toString() + " x " + this.state.height.toString() + "jpg"
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }
  render() {
    return (
      <React.Fragment>
        <div style={{ 'display': 'flex', 'alignItems': 'center', justifyContent: 'center', marginTop: '20px', borderBottom: '1px solid grey', 'paddingBottom': '20px' }}>
          {/* <button>Take a photo</button> */}
          <video id="video" style={{ 'borderRadius': '14px', 'marginRight': '10px', 'boxShadow': '0 0 3px grey' }} />
          <button onClick={() => this.takeScreenshot()} style={{ 'padding': '5px 20px', 'border': 'none', 'boxShadow': '0 0 5px grey', 'borderRadius': '24px' }}>Screenshot</button>
        </div>
        <canvas id="canvas" width={this.state.width} height={this.state.height} style={{ 'display': 'none' }}></canvas>
        <p id="errorMessage" style={{ 'color': 'red' }}></p>
      </React.Fragment>
    )
  }
}
export default Video;