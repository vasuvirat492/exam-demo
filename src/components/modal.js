import './modal.css'
const Modal = (props) => {
    return (
        <div className="backdrop">
            {document.getElementById("body").style.overflow = "hidden"}
            {props.type === "success" ?
                <div className="message-container">
                    <p>You have submitted data succesfully</p>
                    <center><button className="ok-btn" onClick={() => window.location = "/"} style={{ 'backgroundColor': 'green' }}>HOME</button></center>
                </div> :
                <div className="message-container">
                    <center><p className="heading">You have marked as Copy</p></center>
                    <p className="reasons-text">Reasons:</p>
                    {props.summary.clicks > 0 ? <p className="reason">Keyboard key Clicked <span style={{ 'fontSize': '1.2rem' }}>{props.summary.clicks}</span> times</p> : null}
                    {props.summary.apps > 0 ? <p className="reason">Opened other applications <span style={{ 'fontSize': '1.2rem' }}>{props.summary.apps}</span> times </p> : null}
                    {props.summary.blurred > 0 ? <p className="reason">You clicked <span style={{ 'fontSize': '1.2rem' }}>{props.summary.blurred}</span> times on other things</p> : null}
                    <center><button className="ok-btn" onClick={() => window.location = "/"} style={{ 'backgroundColor': 'red' }}>HOME</button></center>
                </div>}
        </div>
    )
}
export default Modal;