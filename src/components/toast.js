import React from 'react';
import './toast.css';
class Toast extends React.Component {
    state = { isFreezed: false, remainingFreezes: [] }
    shouldComponentUpdate(nextProps, nextState) {
        // console.log(nextState)
        // console.log(this.state)
        return !nextState.isFreezed
    }
    freezeExam = () => {
        // console.log('making it happen...')
        document.getElementById("body").style.overflow = "hidden"
        const toastMessageNode = document.createElement("p");
        const messageForParagraph = this.state.remainingFreezes[0].msg + " <br/>You will punished for " + this.state.remainingFreezes[0].time.toString() + " min";
        toastMessageNode.innerHTML = messageForParagraph
        toastMessageNode.classList.add("exam-freeze-message")
        const timerNode = document.createElement("p");
        timerNode.innerHTML = "Time Remaining"
        timerNode.classList.add('timer')
        const toastMessageContainer = document.createElement("div");
        toastMessageContainer.classList.add("exam-freeze")
        toastMessageContainer.appendChild(toastMessageNode)
        toastMessageContainer.appendChild(timerNode)
        document.getElementById("exam-freeze-container").appendChild(toastMessageContainer)
        this.setState({ isFreezed: true })
        let timeRemaining = this.state.remainingFreezes[0].time * 60;
        const timeInterevalRef = setInterval(() => {
            const minutes = Math.floor(timeRemaining / 60);
            const seconds = timeRemaining - (minutes * 60);
            const time = minutes.toString() + " : " + seconds.toString()
            timerNode.innerHTML = "Time Remaining " + time
            timeRemaining = timeRemaining - 1;
            if (timeRemaining < 0) {
                document.getElementById("exam-freeze-container").removeChild(toastMessageContainer)
                this.setState({ isFreezed: false, remainingFreezes: this.state.remainingFreezes.slice(1) })
                document.getElementById("body").style.overflow = "auto"
                clearInterval(timeInterevalRef)
            }
        }, 1000);
    }
    createFreezeExam = (msg, time) => {
        if (this.state.isFreezed) {
            this.createToastMessage("You tried again to copy in punishment. you have to wait another " + time.toString() + " Minutes")
        }
        this.setState({ remainingFreezes: [...this.state.remainingFreezes, { msg, time }] })
    }
    createToastMessage = (msg) => {
        const toastMessageNode = document.createElement("p");
        toastMessageNode.innerHTML = msg
        toastMessageNode.classList.add("toast-message")
        document.getElementById("toast-container").appendChild(toastMessageNode)
        setTimeout(() => document.getElementById("toast-container").removeChild(toastMessageNode), 10000)
    }
    render() {
        return (
            <React.Fragment>
                <div className="exam-freeze-container" id="exam-freeze-container">

                    {/* <p id="exam-freeze-message"></p>
                    <p>time Remaining <span id="time"></span></p> */}
                </div>
                <div className="toast-container" id="toast-container">
                </div>
                {this.state.remainingFreezes.length >= 1 ? this.freezeExam() : null}
            </React.Fragment>
        )
    }
}
export default Toast;