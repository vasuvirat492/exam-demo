import React from 'react';
import Home from './Home';
import VideoComponent from './components/Video'
import './App.css';
class App extends React.Component{
  state = {showExam:false,showButtton:false}
  setShowButton = ()=>{
    this.setState({showButtton:true})
  }
  render(){
    return(
      <span>
        {this.state.showExam ? <Home /> : <div className="center-text">
          <VideoComponent setShowButton={this.setShowButton}/>
          <p>We will track you during exam. So Don't try to copy</p>
          <button onClick={()=>this.setState({showExam:true})} className={this.state.showButtton ? "active-btn" : "disable-btn"}>START EXAM</button>
        </div>}
      </span>
    )
  }
  
}
export default App;
