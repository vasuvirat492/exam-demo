import React from 'react';
import Exam from './components/exam';
import Toast from './components/toast';
import Modal from './components/modal';
import VideoComponent from './components/Video'
import './App.css';
class Home extends React.Component{
  state = {
    previous:{keyboardClicks:0,overlays:0,Missedfocus:0},
    current:{keyboardClicks:0,overlays:0,Missedfocus:0}
  }
  toastRef = React.createRef()
  stopPrntScr() {
    var inpFld = document.createElement("input");
    inpFld.setAttribute("value", ".");
    inpFld.setAttribute("width", "0");
    inpFld.style.height = "0px";
    inpFld.style.width = "0px";
    inpFld.style.border = "0px";
    document.body.appendChild(inpFld);
    inpFld.select();
    document.execCommand("copy");
    inpFld.remove(inpFld);
    console.log('printscreen')
  }
  keyPressed = e =>{
    if(e.key === "Control" || e.key === "Alt" || e.key === "Tab" || e.key === "Shift" || e.key === "Meta"){
      this.setState({current:{...this.state.current,keyboardClicks :this.state.current.keyboardClicks+1}})
    }
    else if(e.keyCode === 44){
      this.stopPrntScr()
    }
  }
  changeVisibilityHandler = () =>{
    if (document.visibilityState !== 'visible') {
      this.setState({current:{...this.state.current,overlays :this.state.current.overlays+1}})
    }
  }
  blurEventHandler = (e)=>{
    if (!e.currentTarget.contains(e.relatedTarget)) {
      this.setState({current:{...this.state.current,Missedfocus :this.state.current.Missedfocus+1}})
    }
    else{
      if(e.relatedTarget.type === 'text'){
        console.log('text field')
      }
      else{
        const totalApp = document.getElementById("total-app")
        totalApp.focus()
      }
    }
  }
  performCopyActions(){
    // console.log('re-rendering..')
    setTimeout(()=>{
      let applications = false;
      let keyPresses = false;
      let mouseClicks = false;
      if(this.state.current.overlays !== 0 && this.state.current.overlays > this.state.previous.overlays){
        applications = true
      }
      if(this.state.current.Missedfocus !== 0 && this.state.current.Missedfocus > this.state.previous.Missedfocus){
        mouseClicks = true
      }
      if(this.state.current.keyboardClicks !== 0 && this.state.current.keyboardClicks > this.state.previous.keyboardClicks){
        keyPresses = true
      }
      if(applications && keyPresses ){
        if(this.state.current.overlays > 1 || this.state.current.keyboardClicks > 1){
          this.toastRef.current.createFreezeExam('you clicked a key opened another application',5)
        }
        else{
          this.toastRef.current.createToastMessage('Last warning ..you clicked a key opened another application to copy')
        }
      }
      else if((applications && mouseClicks) || applications){
        if(this.state.current.overlays > 1 || this.state.current.Missedfocus > 1){
          this.toastRef.current.createFreezeExam('you opened another application',5)
        }
        else{
          this.toastRef.current.createToastMessage('Last warning ..you opened another application to copy')
        }
      }
      else if(mouseClicks && keyPresses){
        if(this.state.current.keyboardClicks > 1 || this.state.current.Missedfocus > 1){
          this.toastRef.current.createFreezeExam('You used your keyboard to open application in small window',5)
        }
        else{
          this.toastRef.current.createToastMessage('Last warning ..you opened application in small window to copy')
        }
      }
      else if(mouseClicks){
        if(this.state.current.Missedfocus > 1){
          this.toastRef.current.createFreezeExam('you clicked on other application or places other than exam',5)
        }
        else{
          this.toastRef.current.createToastMessage('Last warning ..you clicked on other to copy')
        }
      }
      else if(keyPresses){
        if(this.state.current.keyboardClicks > 1){
          this.toastRef.current.createFreezeExam("you pressed key accidentally or intentionally don't do that again",this.state.current.keyboardClicks-1)
        }
        else{
          this.toastRef.current.createToastMessage('Last warning ..you clicked on keyboard copy')
        }
      }
      this.setState({previous:this.state.current})
    },1000)
  }
  suspendEventListeners = ()=>{
    const totalApp = document.getElementById("total-app")
    document.removeEventListener("keyup",this.keyPressed)
    document.removeEventListener("visibilitychange", this.changeVisibilityHandler);
    totalApp.removeEventListener('blur',this.blurEventHandler)
  }
  restartEventListeners = ()=>{
    const totalApp = document.getElementById("total-app")
    totalApp.focus()
    document.addEventListener("keyup",this.keyPressed)
    document.addEventListener("visibilitychange", this.changeVisibilityHandler);
    totalApp.addEventListener('blur',this.blurEventHandler)
  }
  startTimer = ()=>{
    let timeRemaining = 1800;
    const timeIntervals = setInterval(()=>{
      const timeElement = document.getElementById("time");
      const minutes = Math.floor(timeRemaining / 60) ;
      const seconds = timeRemaining - (minutes * 60);
      const time = minutes.toString() + " : "+seconds.toString()
      timeElement.innerHTML = time;
      timeRemaining = timeRemaining - 1;
      if(timeRemaining < 0){
        timeElement.innerHTML = "Time Up";
        clearInterval(timeIntervals)
      }
    },1000)
  }
  componentDidMount(){
    this.startTimer()
    this.restartEventListeners()
  }
  shouldComponentUpdate(nextProps,nextState){
    if(nextState.current.keyboardClicks !==  this.state.current.keyboardClicks){
        return true
    }
    else if(nextState.current.overlays !==  this.state.current.overlays){
      return true
    }
    else if(nextState.current.Missedfocus !==  this.state.current.Missedfocus){
      return true
    }
    else{
      return false
    }
  }
  componentWillUnmount(){
    const totalApp = document.getElementById("total-app")
    document.removeEventListener("keyup",this.keyPressed)
    document.removeEventListener("visibilitychange", this.changeVisibilityHandler);
    totalApp.removeEventListener('blur',this.blurEventHandler)
  }
  render(){
    return (
      <div className="container" id="total-app" tabIndex="0">
        {this.state.current.keyboardClicks >= 1 || this.state.current.overlays >= 1 || this.state.current.Missedfocus >=1 ?
          <nav>
            <div className="navbar">
              <span className="logo">Secure Exams</span>
              <span>Time Remaining : <span id="time"></span></span>
            </div>
            <p className="error-msg" style={{'color':'rgba(255, 0, 0)'}}>You already tried to copy. If you do again will mark this as copy</p>
          </nav>
          :
          <nav>
          <div className="navbar">
            <span className="logo">Secure Exams</span>
            <span>Time Remaining : <span id="time"></span></span>
          </div>
          <p className="error-msg">Don't try to copy. We can catch you easily</p>
        </nav>
        }
        <Toast ref={this.toastRef}/>
        <VideoComponent />
        <Exam />
        {this.state.current.keyboardClicks >= 6 || this.state.current.overlays >= 3 || this.state.current.Missedfocus >=3 ?           
            <React.Fragment>
             {this.suspendEventListeners()}
             <Modal type="copied" summary={{'clicks':this.state.current.keyboardClicks,'apps':this.state.current.overlays,'blurred':this.state.current.Missedfocus}}/>
           </React.Fragment>
           :null}
           {this.performCopyActions()}
      </div>
    )
    }
  
}
export default Home;
